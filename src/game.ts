//import {BuilderHUD} from './modules/BuilderHUD'

const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floorBaseDirt_02 = new Entity()
floorBaseDirt_02.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseDirt_02/FloorBaseDirt_02.glb')
floorBaseDirt_02.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseDirt_02.addComponentOrReplace(transform_2)
engine.addEntity(floorBaseDirt_02)

const rockLarge_02 = new Entity()
rockLarge_02.setParent(scene)
const gltfShape_2 = new GLTFShape('models/RockLarge_02/RockLarge_02.glb')
rockLarge_02.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(13.5, 0, 12.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockLarge_02.addComponentOrReplace(transform_3)
engine.addEntity(rockLarge_02)

const rockLarge_01 = new Entity()
rockLarge_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/RockLarge_01/RockLarge_01.glb')
rockLarge_01.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  position: new Vector3(4, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockLarge_01.addComponentOrReplace(transform_4)
engine.addEntity(rockLarge_01)

const rockMedium_03 = new Entity()
rockMedium_03.setParent(scene)
const gltfShape_4 = new GLTFShape('models/RockMedium_03/RockMedium_03.glb')
rockMedium_03.addComponentOrReplace(gltfShape_4)
const transform_5 = new Transform({
  position: new Vector3(12, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockMedium_03.addComponentOrReplace(transform_5)
engine.addEntity(rockMedium_03)

const rockMedium_02 = new Entity()
rockMedium_02.setParent(scene)
const gltfShape_5 = new GLTFShape('models/RockMedium_02/RockMedium_02.glb')
rockMedium_02.addComponentOrReplace(gltfShape_5)
const transform_6 = new Transform({
  position: new Vector3(10.5, 0, 9.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockMedium_02.addComponentOrReplace(transform_6)
engine.addEntity(rockMedium_02)

const rockMedium_03_2 = new Entity()
rockMedium_03_2.setParent(scene)
rockMedium_03_2.addComponentOrReplace(gltfShape_4)
const transform_7 = new Transform({
  position: new Vector3(2.5, 0, 12.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockMedium_03_2.addComponentOrReplace(transform_7)
engine.addEntity(rockMedium_03_2)

const rockSmall_01 = new Entity()
rockSmall_01.setParent(scene)
const gltfShape_6 = new GLTFShape('models/RockSmall_01/RockSmall_01.glb')
rockSmall_01.addComponentOrReplace(gltfShape_6)
const transform_8 = new Transform({
  position: new Vector3(8.5, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockSmall_01.addComponentOrReplace(transform_8)
engine.addEntity(rockSmall_01)

const rockSmall_03 = new Entity()
rockSmall_03.setParent(scene)
const gltfShape_7 = new GLTFShape('models/RockSmall_03/RockSmall_03.glb')
rockSmall_03.addComponentOrReplace(gltfShape_7)
const transform_9 = new Transform({
  position: new Vector3(13.5, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockSmall_03.addComponentOrReplace(transform_9)
engine.addEntity(rockSmall_03)

const rockSmall_01_2 = new Entity()
rockSmall_01_2.setParent(scene)
rockSmall_01_2.addComponentOrReplace(gltfShape_6)
const transform_10 = new Transform({
  position: new Vector3(14, 0, 1.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockSmall_01_2.addComponentOrReplace(transform_10)
engine.addEntity(rockSmall_01_2)

const rockSmall_02 = new Entity()
rockSmall_02.setParent(scene)
const gltfShape_8 = new GLTFShape('models/RockSmall_02/RockSmall_02.glb')
rockSmall_02.addComponentOrReplace(gltfShape_8)
const transform_11 = new Transform({
  position: new Vector3(5.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockSmall_02.addComponentOrReplace(transform_11)
engine.addEntity(rockSmall_02)

let rover = new Entity()
let roverShape = new GLTFShape("models/rover.glb")
rover.addComponent(roverShape)
rover.addComponent(new Transform({ 
position: new Vector3(12.74,0.64,10.96),
rotation:  Quaternion.Euler(-4.343,-16.49,29.875),
scale: new Vector3(1, 1, 1)
}))
engine.addEntity(rover)

//const hud:BuilderHUD =  new BuilderHUD()
//hud.attachToEntity(rover)
//hud.setDefaultParent(scene)

//0,0:  Existing(12.74,0.64,10.96,  -4.343,-16.49,29.875,  1,1,1)
